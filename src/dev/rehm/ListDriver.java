package dev.rehm;

import java.util.*;

public class ListDriver {

    public static void main(String[] args) {
        // instantiate an ArrayList object
        List<String> stringList = new ArrayList<>();

        // add elements to our array lit
        stringList.add("hello");
        stringList.add("bananas");
        stringList.add("apples");
        stringList.add("oranges");
//        System.out.println(stringList);
//        System.out.println(stringList.get(2));
//        System.out.println(stringList.contains("lemon"));

        // using a traditional for loop
//        for(int i=0;i<stringList.size();i++){
//            System.out.println(stringList.get(i));
//        }

        // using a for each loop
//        for(String s: stringList){
//            System.out.println(s);
//        }

        // using an iterator to remove elements
        Iterator<String> stringIterator =  stringList.iterator();
        while(stringIterator.hasNext()){
            String currentString = stringIterator.next();
            if(currentString.length()>6){
                stringIterator.remove();
            }
        }

//        System.out.println(stringList);

        Queue<String> stringQueue = new LinkedList<>();
        stringQueue.add("alpha");
        stringQueue.add("bravo");
        stringQueue.add("charlie");
        System.out.println(stringQueue);
        String polledString1 = stringQueue.poll();
        System.out.println("removed "+polledString1+". The remaining queue contains: "+stringQueue);

        String polledString2 = stringQueue.poll();
        System.out.println("removed "+polledString2+". The remaining queue contains: "+stringQueue);



    }

}
