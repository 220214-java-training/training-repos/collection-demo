package dev.rehm.models;

import java.util.Set;
import java.util.TreeSet;

public class UserDriver {

    public static void main(String[] args) {
        Set<User> users = new TreeSet<>();
        users.add(new User(2,"sjenkins","supersecret"));
        users.add(new User(4,"tjones","pass123"));
        users.add(new User(11,"lsmith","fjkakdlaf"));
        System.out.println(users);
    }
}
