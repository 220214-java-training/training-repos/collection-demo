package dev.rehm.mycollections;

public interface List {

    public boolean contains(int value);
    public boolean add(int newValue);
    public void remove(int index);
    public int get(int index);

}
