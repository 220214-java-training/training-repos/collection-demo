package dev.rehm;

import java.util.HashMap;
import java.util.Map;

public class MapDriver {

    public static void main(String[] args) {
        Map<String,String> credentials = new HashMap<>();
        credentials.put("crehm", "p4ssw0rd");
        credentials.put("johndoe", "pass123");
        credentials.put("sallyjenkins", "123456");

        /*
        System.out.println(credentials);
        System.out.println(credentials.keySet());
        System.out.println(credentials.values());

        String username = "crehm";
        String newPass = "anotherpass";
        System.out.println(username.hashCode());
        credentials.put(username, newPass);
        System.out.println(credentials);
         */

        for(String str : credentials.keySet()){ // ['crehm', 'johndoe', 'sallyjenkins']
            System.out.println("key="+str+", value="+credentials.get(str));
        }

    }
}
