package dev.rehm;

import java.util.ArrayList;
import java.util.List;

public class GenericDriver {

    public static void main(String[] args) {
        /*
            Generics allow us to provide a type as a parameter
         */
        List<String> stringList = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>();
        stringList.add("hello");
//        integerList.add("hello");
        integerList.add(3);

        String myString = stringList.get(0);
        int myInt = integerList.get(0);

        List list = new ArrayList();
        list.add(new Object());
        list.add("hello world!!");
        list.add(24);
        System.out.println(list);

        String s = (String) list.get(0); // we get a ClassCastException here
        s.toCharArray();
    }
}
