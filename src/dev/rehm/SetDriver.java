package dev.rehm;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDriver {

    public static void main(String[] args) {
        Set<String> stringSet = new HashSet<>();
        stringSet.add("hello");
        stringSet.add("welcome to our set demo");
        stringSet.add("hello again");
        stringSet.add("hello");
        System.out.println(stringSet);

        Set<String> stringTSet = new TreeSet<>(new ReversedStringComparator());
        stringSet.add("hello");
        stringTSet.add("welcome to our set demo");
        stringTSet.add("hello again");
        stringTSet.add("hello");
        System.out.println(stringTSet);
    }

}
